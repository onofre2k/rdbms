
package inscripciones;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import org.postgresql.jdbc.PgConnection;


public class Persona extends javax.swing.JFrame {
    public static final String URL = "jdbc:postgresql://localhost:5432/aonofre";
    public static final String USERNAME = "postgres";
    public static final String PASSWD = "Sonoramico12";
    PreparedStatement prepared;
    ResultSet result;

    public Persona() {
        initComponents();
        textIdPersona.setVisible(false);
        setLocationRelativeTo(null);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        botonInsertar = new javax.swing.JButton();
        botonModificar = new javax.swing.JButton();
        botonEliminar = new javax.swing.JButton();
        textNombre = new javax.swing.JTextField();
        textPaterno = new javax.swing.JTextField();
        textMaterno = new javax.swing.JTextField();
        textBuscar = new javax.swing.JTextField();
        botonBuscar = new javax.swing.JButton();
        labelNombre = new javax.swing.JLabel();
        labelPaterno = new javax.swing.JLabel();
        labelMaterno = new javax.swing.JLabel();
        textIdPersona = new javax.swing.JTextField();
        botonLimpiar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        botonInsertar.setText("Insertar");
        botonInsertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonInsertarActionPerformed(evt);
            }
        });

        botonModificar.setText("Modificar");
        botonModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonModificarActionPerformed(evt);
            }
        });

        botonEliminar.setText("Eliminar");
        botonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarActionPerformed(evt);
            }
        });

        botonBuscar.setText("Buscar");
        botonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBuscarActionPerformed(evt);
            }
        });

        labelNombre.setText("Nombre:");

        labelPaterno.setText("Apellido Paterno:");

        labelMaterno.setText("Apellido Materno:");

        botonLimpiar.setText("Limpiar");
        botonLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonLimpiarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(textBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(textIdPersona)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(botonBuscar))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelNombre, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(labelPaterno, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(labelMaterno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(botonInsertar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                                .addComponent(botonModificar)
                                .addGap(6, 6, 6)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textMaterno)
                            .addComponent(textNombre)
                            .addComponent(textPaterno)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addComponent(botonEliminar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                                .addComponent(botonLimpiar)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscar)
                    .addComponent(textIdPersona, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelNombre))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelPaterno))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelMaterno))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonInsertar)
                    .addComponent(botonModificar)
                    .addComponent(botonEliminar)
                    .addComponent(botonLimpiar))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonInsertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonInsertarActionPerformed
        PgConnection conexion = null;
        
        try {
            conexion = getConnection();
            prepared = conexion.prepareStatement("INSERT INTO persona(pers_nombre, pers_apaterno, pers_amaterno) VALUES(?,?,?)");
            prepared.setString(1, textNombre.getText());
            prepared.setString(2, textPaterno.getText());
            prepared.setString(3, textMaterno.getText());
            int resultado = prepared.executeUpdate();
            if(resultado > 0) {
                JOptionPane.showMessageDialog(null, "Se registró correctamente la persona");
                limpiar();
            } else {
                JOptionPane.showMessageDialog(null, "Error al registrarse la persona");
                //limpiar();   
            }
            conexion.close();
        } catch (Exception e) {
            System.err.println("Error: "+e);
        }
        
    }//GEN-LAST:event_botonInsertarActionPerformed

    private void botonLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonLimpiarActionPerformed
        limpiar();
    }//GEN-LAST:event_botonLimpiarActionPerformed

    private void botonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBuscarActionPerformed
        PgConnection conexion = null;
        
        try {
            conexion = getConnection();
            prepared = conexion.prepareStatement("SELECT * FROM persona WHERE pers_id_persona=?");
            prepared.setInt(1, Integer.valueOf(textBuscar.getText()));
            result = prepared.executeQuery();
            
            if(result.next()) {
                textIdPersona.setText(String.valueOf(result.getInt("pers_id_persona")));
                textNombre.setText(result.getString("pers_nombre"));
                textPaterno.setText(result.getString("pers_apaterno"));
                textMaterno.setText(result.getString("pers_amaterno"));
            } else {
                JOptionPane.showMessageDialog(null, "Error: No se encuentra una persona con el ID");
            }
            
            conexion.close();
        } catch (Exception e) {
            System.err.println("Error: "+e);
        }
    }//GEN-LAST:event_botonBuscarActionPerformed

    private void botonModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonModificarActionPerformed
        PgConnection conexion = null;
        
        try {
            conexion = getConnection();
            prepared = conexion.prepareStatement("UPDATE persona SET pers_nombre=?, pers_apaterno=?, pers_amaterno=? WHERE pers_id_persona=?");
            prepared.setString(1, textNombre.getText());
            prepared.setString(2, textPaterno.getText());
            prepared.setString(3, textMaterno.getText());
            prepared.setInt(4, Integer.valueOf(textIdPersona.getText()));
            int resultado = prepared.executeUpdate();
            if(resultado > 0) {
                JOptionPane.showMessageDialog(null, "Se actualizó correctamente la persona");
                //limpiar();
            } else {
                JOptionPane.showMessageDialog(null, "Error al actualizarse la persona");
                //limpiar();   
            }
            conexion.close();
        } catch (Exception e) {
            System.err.println("Error: "+e);
        }
    }//GEN-LAST:event_botonModificarActionPerformed

    private void botonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarActionPerformed
        PgConnection conexion = null;
        
        try {
            conexion = getConnection();
            prepared = conexion.prepareStatement("DELETE FROM persona WHERE pers_id_persona=?");
            prepared.setInt(1, Integer.valueOf(textIdPersona.getText()));
            int resultado = prepared.executeUpdate();
            if(resultado > 0) {
                JOptionPane.showMessageDialog(null, "Se eliminó correctamente la persona");
                //limpiar();
            } else {
                JOptionPane.showMessageDialog(null, "Error al eliminar la persona");
                //limpiar();   
            }
            conexion.close();
        } catch (Exception e) {
            System.err.println("Error: "+e);
        }
    }//GEN-LAST:event_botonEliminarActionPerformed

    public PgConnection getConnection() {
        PgConnection conexion = null;
        
        try {
            Class.forName("org.postgresql.Driver");
            conexion = (PgConnection) DriverManager.getConnection(URL, USERNAME, PASSWD);
            //JOptionPane.showMessageDialog(null, "Conexión Exitosa");
        } catch (Exception e) {
            System.err.println("Error" + e);
        }
        
        return conexion;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Persona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Persona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Persona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Persona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Persona().setVisible(true);
            }
        });
    }
    
    public void limpiar() {
        textBuscar.setText(null);
        textNombre.setText(null);
        textPaterno.setText(null);
        textMaterno.setText(null);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonBuscar;
    private javax.swing.JButton botonEliminar;
    private javax.swing.JButton botonInsertar;
    private javax.swing.JButton botonLimpiar;
    private javax.swing.JButton botonModificar;
    private javax.swing.JLabel labelMaterno;
    private javax.swing.JLabel labelNombre;
    private javax.swing.JLabel labelPaterno;
    private javax.swing.JTextField textBuscar;
    private javax.swing.JTextField textIdPersona;
    private javax.swing.JTextField textMaterno;
    private javax.swing.JTextField textNombre;
    private javax.swing.JTextField textPaterno;
    // End of variables declaration//GEN-END:variables
}
